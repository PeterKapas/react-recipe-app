import styled from 'styled-components'
import { motion } from 'framer-motion'
import { Link, useParams } from 'react-router-dom'
import { useEffect, useState } from 'react'

const Cuisine = () => {
	const [cuisine, setCuisine] = useState([])
	let params = useParams()

	const getCuisine = async (name) => {
		const data = await fetch(
			`https://api.spoonacular.com/recipes/complexSearch?apiKey=050b499e2c9d42f3be0fea934a41055e&cuisine=${name}`
		)
		const recipes = await data.json()
		setCuisine(recipes.results)
	}

	useEffect(() => {
		getCuisine(params.type)
	}, [params.type])
	return (
		<Grid
			animate={{ opacity: 1 }}
			initial={{ opacity: 0 }}
			exit={{ opacity: 0 }}
			transition={{ duration: 0.7 }}
		>
			{cuisine.map((item) => {
				return (
					<Link to={`/recipe/${item.id}`}>
						<Card key={item.id}>
							<img src={item.image} alt='item.title' />
							<h4>{item.title}</h4>
						</Card>
					</Link>
				)
			})}
		</Grid>
	)
}

const Grid = styled(motion.div)`
	display: grid;
	grid-template-columns: repeat(auto-fit, minmax(20rem, 1fr));
	grid-gap: 3rem;
`
const Card = styled.div`
	img {
		width: 100%;
		border-radius: 2rem;
	}
	a {
		text-decoration: none;
	}
	h4 {
		text-align: center;
		padding: 1rem;
	}
`

export default Cuisine
